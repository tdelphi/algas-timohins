angular.module('algas', ['ngResource'])
  .controller('algas-js', ["$scope", function($scope) {
    $scope.greeting = {id: 'xxx', content: 'Hello World!'};
    $scope.message = "Hello World!";

    $scope.$watch("bruto", function (newValue) {
        var social = newValue * 10.05/100;
        var IIN = (newValue - social - 75) * 23/100;
        $scope.neto = newValue - social - IIN;

    });
    $scope.bruto = 2000;

}])
  .controller('solis2', ["$scope", "$resource", function($scope, $resource) {
    var solis2calc = $resource("solis2");
    $scope.getValue = function (bruto) {
        $scope.calcResult = solis2calc.get({bruto: bruto})
    }
    $scope.greeting = {id: 'xxx', content: 'Hello World!'};
    $scope.message = "Hello World!";
    //$scope.message = $scope.getValue(bruto: 300);


    $scope.$watch("bruto", function (newValue) {
        $scope.getValue(newValue);
    });
    $scope.bruto = 2000;
}]);