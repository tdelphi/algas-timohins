package aleksejs.timohins.homework;

import aleksejs.timohins.homework.solis2.CalculationResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;

import static aleksejs.timohins.homework.BigDecimal.Utils.percent;
import static java.math.BigDecimal.valueOf;

@Controller
public class AlgasControlller {

    @RequestMapping("/")
    String home() {
        return "/algas.html";
    }

    @RequestMapping("/solis2")
    @ResponseBody
    CalculationResult solis2(@RequestParam("bruto") String brutoParam) {
        DecimalFormat fmt = new DecimalFormat();

        BigDecimal bruto = null;
        fmt.setParseBigDecimal(true);
        try {
            bruto = (BigDecimal) fmt.parse(brutoParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        BigDecimal social = percent(bruto, valueOf(10.05));
        BigDecimal IIN = percent(bruto.subtract(social).subtract(valueOf(75)), 23);
        BigDecimal neto = bruto.subtract(social).subtract(IIN);
        return CalculationResult.createNew(neto);
    }
}
