package aleksejs.timohins.homework.BigDecimal;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

public class Utils {
    public static BigDecimal percent(BigDecimal value, BigDecimal percent) {
        return value.multiply(percent).divide(valueOf(100));
    }

    public static BigDecimal percent(BigDecimal value, int percent) {
        return value.multiply(valueOf(percent)).divide(valueOf(100));
    }

}
