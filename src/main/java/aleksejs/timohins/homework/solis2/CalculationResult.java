package aleksejs.timohins.homework.solis2;

import java.math.BigDecimal;

public class CalculationResult {

    public String neto;

    public static CalculationResult createNew(BigDecimal neto) {
        CalculationResult result = new CalculationResult();
        result.neto = neto.toString();
        return result;
    }
}
